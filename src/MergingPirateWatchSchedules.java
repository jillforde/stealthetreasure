/**
 * Objective: Write an algorithm that takes a list of meeting time ranges
 *              and returns a condensed list of watch time ranges of all of the pirates.
 *
 *
 *      Each pirate watches the treasure in a 30 minute interval, represented as an integer
 *              ex: 0 = midnight, 1 = 12:30am
 *
 * Example: If pirates start and end times are   [(0, 1), (3, 5), (4, 8), (10, 12), (9, 10)]
 * Your function would return [(0, 1), (3, 8), (9, 12)]
 *
 *      Do not assume that watch times are in order.
 *
 */
public class MergingPirateWatchSchedules {


}
