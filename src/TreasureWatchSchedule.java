/**
 * POJO for a Schedule in which the pirates will watch the treasure.
 */
public class TreasureWatchSchedule {
    int startTime;
    int endTime;

    public TreasureWatchSchedule(int startTime, int endTime){
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
